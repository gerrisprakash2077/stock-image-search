const accessKey = 'WeoEdSQZhiHOnupCqq5cenGimJkLZRqzlw9t4X-qh8k'
const inpSearch = document.getElementById('search')
const body = document.querySelector('body')
const main = document.querySelector('main')

let pageNumber = 1;
let searchHistory = false;
//random images
rendeRandom()

inpSearch.addEventListener('keyup', (event) => {
    if (event.key == 'Enter' && inpSearch.value != "") {
        searchQuery(inpSearch.value, false, 0)
        searchHistory = true;
    }
})
document.querySelector('svg').addEventListener('click', (event) => {
    if (inpSearch.value != "") {
        searchQuery(inpSearch.value, false, 0)
        searchHistory = true
    }
})
window.addEventListener('click', () => {
    if (searchHistory == true) {
        searchQuery(inpSearch.value, true, ++pageNumber)
    } else {
        rendeRandom()
    }
})

function rendeRandom() {
    fetch(`https://api.unsplash.com/photos/random?client_id=${accessKey}&count=10`).then((data) => {
        return data.json()
    }).then((data) => {
        data.forEach(element => {
            createUI(element)
        });
    })
}
function searchQuery(search, deleteFlag, pageNumber) {
    let url = `https://api.unsplash.com/search/photos?page=1&query=${search}&client_id=${accessKey}&per_page=10`
    if (pageNumber) {
        url = `https://api.unsplash.com/search/photos?page=${pageNumber}&query=${search}&client_id=${accessKey}&per_page=10`
    }
    fetch(url).then((data) => {
        return data.json()
    }).then((data) => {
        if (deleteFlag == false) {
            Array.from(main.children).forEach((element) => {
                element.remove()
            })
        }
        data.results.forEach(element => {
            createUI(element)
        });
    })
}
function createUI(element) {
    let img = document.createElement('div')
    let divv = document.createElement('div')
    let name = document.createElement('p')
    let likes = document.createElement('p')
    let profile = document.createElement('img')
    profile.src = element.user.profile_image.medium
    likes.innerText = element.likes + " likes"
    name.innerText = element.user.name

    divv.setAttribute('class', 'pop')
    divv.style.background = "rgba(0, 0, 0, .3) "
    divv.style.color = "white"
    divv.style.padding = "15px"
    profile.style.float = "right"
    name.style.lineHeight = "24px"
    likes.style.lineHeight = "24px"
    profile.style.borderRadius = "20px"
    profile.style.width = "40px"
    img.style.position = "relative"

    let aLink = document.createElement('a')
    aLink.style.cursor = "pointer"
    aLink.href = element.user.portfolio_url
    aLink.append(profile)
    divv.append(aLink)
    divv.append(name)
    divv.append(likes)
    divv.style.position = "absolute"
    divv.style.bottom = "0px"
    divv.style.width = "370px"
    img.append(divv)
    img.style.backgroundImage = `url('${element.urls.regular}')`
    img.style.backgroundSize = "cover"
    img.style.backgroundPosition = "center"
    img.style.width = "400px"
    img.style.height = "300px"
    img.style.display = "block"
    img.setAttribute('class', 'cards')
    main.append(img)
}

